# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Specialist.create(name: 'Dr. Harry Vex', speciality: 'Foot')
Specialist.create(name: 'Dr. Jamal Malik', speciality: 'Head')
Specialist.create(name: 'Dr. Roy Anderson', speciality: 'Spine')

Patient.create(name: 'Ryan Jackson', street_address: '9813 Hemington Way')
Patient.create(name: 'David Nguyen', street_address: '4726 Laraden Court')
Patient.create(name: 'Rahul Sharma', street_address: '6817 Colton Cove Drive')
Patient.create(name: 'Mohammad Sultan', street_address: '3298 Garden Hill Road')
Patient.create(name: 'Roger Smith', street_address: '13981 Equinox Lane')

appointment_list = [
    [1,1,"Twisted my ankle",'2014-10-03',35],
    [2,1,"Headache",'2014-10-14',50],
    [3,2,"Back has been hurting",'2014-10-10',40],
    [1,2,"Can't put pressure on left foot",'2014-10-15',25],
    [2,3,"Constant migraines",'2014-10-07',30],
    [3,3,"Backache at night",'2014-10-19',25],
    [1,4,"Cant run fast",'2014-10-24',25],
    [2,4,"Seeing double of everything",'2014-10-23',40],
    [3,5,"Cant move neck all the way",'2014-10-27',27],
    [1,5,"Need medicine to jump higher",'2014-10-24',40]
]

appointment_list.each do |specialist_id,patient_id,complaint,appointment_date,fee|
  Appointment.create(specialist_id: specialist_id,patient_id: patient_id,complaint: complaint,
                     appointment_date:appointment_date,fee:fee)
end

insurance_list = [
    ["Aetna","60923 Sir Roger Court"],
    ["Humana","8213 Jagery Road"],
    ["Assurant Health","513 Gnome Lane"],
    ["United Health One","9314 Daemon Hill"],
    ["Kaiser Permanente","1894 Long Ridge Drive"]
]

insurance_list.each do |name,street_address|
  Insurance.create(name: name,street_address: street_address)
end

