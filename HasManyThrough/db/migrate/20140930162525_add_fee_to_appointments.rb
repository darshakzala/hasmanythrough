class AddFeeToAppointments < ActiveRecord::Migration
  def change
    add_column :appointments, :fee, :integer
  end
end
