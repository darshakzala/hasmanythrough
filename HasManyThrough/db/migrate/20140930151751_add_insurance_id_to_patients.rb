class AddInsuranceIdToPatients < ActiveRecord::Migration
  def change
    add_column :patients, :insurance_id, :integer
  end
end
